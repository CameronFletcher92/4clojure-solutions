(ns solutions)

;; 4clojure.com/problem/20
(defn second-to-last
  [ls]
  (nth ls (- (count ls) 2)))

;; 4clojure.com/problem/21
(defn nth-elem
  [coll n]
  (get (vec coll) n))

;; 4clojure.com/problem/22
(defn count-elems
  [coll]
  (reduce (fn [x y] (inc x)) 0 coll))

;; 4clojure.com/problem/23
(defn reverse-sequence
  [coll]
  (reduce conj '() coll))

;; 4clojure.com/problem/24
(defn sum-elems
  [nums]
  (reduce + nums))

;; 4clojure.com/problem/25
(defn find-odd
  [nums]
  (filter odd? nums))

;; 4clojure.com/problem/26
(defn first-n-fib
  [n]
  (letfn [(fib [n]
            (if (> n 2)
              (+ (fib (- n 2)) (fib (- n 1)))
              1))]
    (map fib (range 1 (+ n 1)))))

;; 4clojure.com/problem/26 - MEMOIZED
(def fib-mem
  (memoize
    (fn [x]
      (if (< x 2)
        x
        (bigint (+ (fib-mem (- x 1))
                   (fib-mem (- x 2))))))))

(defn first-n-fib
  [n]
  (map fib-mem (range (+ n 1))))

;; 4clojure.com/problem/27
(defn palindrome?
  [coll]
  (= (vec coll) (rseq (vec coll))))

;; 4clojure.com/problem/156
(defn map-defaults
  [v ks]
  (reduce #(assoc %1 %2 v) {} ks))
